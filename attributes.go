package store

import (
	"encoding/json"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
)

var Empty = errors.New("no more attributes")

type Attributes map[string]interface{}

func (attr Attributes) BSON() bson.M {
	return bson.M(attr)
}

func (attr Attributes) JSON() ([]byte, error) {
	return json.Marshal(attr)
}

func (attr Attributes) Iter() bson.M {
	return attr.BSON()
}
