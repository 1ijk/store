package store

type Action struct {
	Closure func() Result
}

func (a Action) Do() Result {
	return a.Closure()
}
