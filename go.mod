module gitlab.com/1ijk/store

go 1.16

require (
	github.com/google/uuid v1.2.0
	go.mongodb.org/mongo-driver v1.5.3
)
