package store

import "fmt"

type Kind fmt.Stringer

type kind struct{}

func (kind) String() string {
	return "default"
}

var DefaultKind = kind{}
