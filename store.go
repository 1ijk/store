package store

import (
	"context"
)

type Store interface {
	FindBy(context.Context, findByOptions) Action
	Create(context.Context, createOptions) Action
	Update(context.Context, updateOptions) Action
	Delete(context.Context, deleteOptions) Action
}
