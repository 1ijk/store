package store

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

var (
	ErrOptionModel      = errors.New("options for Model required")
	ErrOptionUUID       = errors.New("options for UUID required")
	ErrOptionAttributes = errors.New("options for Attributes required")
	ErrOptionKind       = errors.New("options for Kind required")
)

type createOptions *options

func CreateWith() createOptions {
	return NewOptions().Require("kind", "attributes")
}

type updateOptions *options

func UpdateWith() updateOptions {
	return NewOptions().Require("uuid", "attributes")
}

type findByOptions *options

func Scope() findByOptions {
	return NewOptions().Require("kind", "attributes")
}

type deleteOptions *options

func DeleteWith() deleteOptions {
	return NewOptions().Require("uuid", "kind")
}

type options struct {
	required   []string
	uuid       *uuid.UUID
	kind       Kind
	attributes Attributes
}

func NewOptions() *options {
	return &options{
		required:   []string{},
		uuid:       nil,
		kind:       nil,
		attributes: nil,
	}
}

func (opt *options) Validate() error {
	for _, parameter := range opt.required {
		switch parameter {
		case "uuid":
			if opt.uuid == nil {
				return ErrOptionUUID
			}
		case "kind":
			if opt.kind == nil {
				return ErrOptionKind
			}
		case "attributes":
			if opt.attributes == nil {
				return ErrOptionKind
			}
		default:
			if opt.attributes == nil {
				return fmt.Errorf("cannot require %s without attributes", parameter)
			}

			if _, ok := opt.attributes[parameter]; !ok {
				return fmt.Errorf("attribute for %s not found", parameter)
			}
		}
	}
	return nil
}

func (opt *options) Require(requirements ...string) *options {
	opt.required = append(opt.required, requirements...)
	return opt
}

func (opt *options) UUID(uuid uuid.UUID) *options {
	opt.uuid = &uuid
	return opt
}

func (opt *options) Model(model Model) *options {
	opt.kind = model.Kind()
	opt.attributes = model.Attributes()
	return opt
}

func (opt *options) Attributes(attributes Attributes) *options {
	opt.attributes = attributes
	return opt
}

func (opt *options) Kind(kind Kind) *options {
	opt.kind = kind
	return opt
}
