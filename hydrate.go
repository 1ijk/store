package store

type Hydrate interface {
	Hydrate(Attributes) error
}
