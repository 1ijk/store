package store

type Model interface {
	Kind() Kind
	Attributes() Attributes
}
