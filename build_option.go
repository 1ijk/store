package store

import (
	"fmt"
	"github.com/google/uuid"
	"reflect"
	"time"
)

type BuildOption func(*Record) error

func UUID(uuid uuid.UUID) BuildOption {
	return func(record *Record) error {
		record.UUID = uuid
		return nil
	}
}

func StringUUID(s string) BuildOption {
	return func(record *Record) error {
		uuid, err := uuid.Parse(s)
		if err != nil {
			return err
		}

		record.UUID = uuid
		return nil
	}
}

func CreatedAt(t time.Time) BuildOption {
	return func(record *Record) error {
		record.CreatedAt = &t
		return nil
	}
}

func UpdatedAt(t time.Time) BuildOption {
	return func(record *Record) error {
		record.UpdatedAt = &t
		return nil
	}
}

func TimestampCreatedAt(ts string) BuildOption {
	return func(record *Record) error {
		t, err := time.Parse(time.RFC3339, ts)
		if err != nil {
			return err
		}
		record.CreatedAt = &t
		return nil
	}
}

func TimestampUpdatedAt(ts string) BuildOption {
	return func(record *Record) error {
		t, err := time.Parse(time.RFC3339, ts)
		if err != nil {
			return err
		}
		record.UpdatedAt = &t
		return nil
	}
}

func FromModel(model Model) BuildOption {
	return func(record *Record) error {
		record.Kind = model.Kind()
		record.Attributes = model.Attributes()
		return nil
	}
}

func Collection(kind Kind) BuildOption {
	return func(record *Record) error {
		record.Kind = kind
		return nil
	}
}

func Data(attributes Attributes) BuildOption {
	return func(record *Record) error {
		for name, attribute := range attributes.Iter() {
			existing, ok := record.Attributes[name]
			if !ok {
				return fmt.Errorf("attribute %s does not exist on the record", name)
			}
			if reflect.TypeOf(existing) != reflect.TypeOf(attribute) {
				return fmt.Errorf("attribute data has differente types: %t given but %t present on record", attribute, existing)
			}
			record.Attributes[name] = attribute
		}
		return nil
	}
}
