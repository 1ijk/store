package store

import (
	"github.com/google/uuid"
	"time"
)

type Record struct {
	uuid.UUID
	CreatedAt *time.Time
	UpdatedAt *time.Time
	Kind
	Attributes
}

func build(options ...BuildOption) (*Record, error) {
	ts := time.Now()
	record := Record{
		UUID:       uuid.New(),
		CreatedAt:  &ts,
		UpdatedAt:  &ts,
		Kind:       DefaultKind,
		Attributes: Attributes{},
	}

	for _, option := range options {
		if err := option(&record); err != nil {
			return nil, err
		}
	}

	return &record, nil
}
