package store

import "errors"

var (
	ErrNoResult = errors.New("no result")
)

type Result interface {
	Hydrate(Hydrate) error
	OK() bool
}

type createResult struct {
	record *Record
	err    error
}

func (r createResult) Hydrate(model Hydrate) error {
	if r.record == nil {
		return ErrNoResult
	}
	return model.Hydrate(r.record.Attributes)
}

func (r createResult) OK() bool {
	return r.err == nil
}

type updateResult struct {
	err error
}

func (r updateResult) Hydrate(_ Hydrate) error {
	return ErrNoResult
}

func (r updateResult) OK() bool {
	return r.err == nil
}

type findByResult struct {
	record *Record
	err    error
}

func (r findByResult) Hydrate(model Hydrate) error {
	if r.record == nil {
		return ErrNoResult
	}

	return model.Hydrate(r.record.Attributes)
}

func (r findByResult) OK() bool {
	return r.err == nil
}

type deleteResult struct {
	err error
}

func (r deleteResult) Hydrate(model Hydrate) error {
	return ErrNoResult
}

func (r deleteResult) OK() bool {
	return r.err == nil
}
