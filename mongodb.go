package store

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoDB struct {
	db          *mongo.Database
	collections map[Kind]*mongo.Collection
}

func StoreWithMongoDB(db *mongo.Database) Store {
	return &MongoDB{db: db, collections: map[Kind]*mongo.Collection{}}
}

func (m *MongoDB) collection(k Kind) *mongo.Collection {
	_, ok := m.collections[k]
	if !ok {
		m.collections[k] = m.db.Collection(k.String())
	}
	return m.collections[k]
}

func (m *MongoDB) Create(ctx context.Context, opt createOptions) Action {
	return Action{
		Closure: func() Result {
			var result createResult

			if err := (*options)(opt).Validate(); err != nil {
				result.err = err
				return result
			}

			record, err := build(
				Collection(opt.kind),
				Data(opt.attributes),
			)
			if err != nil {
				result.err = err
				return result
			}

			if _, err = m.collection(opt.kind).InsertOne(ctx, record); err != nil {
				result.err = err
				return result
			}

			result.record = record

			return result
		},
	}
}

func (m *MongoDB) Update(ctx context.Context, opt updateOptions) Action {
	return Action{
		Closure: func() Result {
			var result updateResult

			if err := (*options)(opt).Validate(); err != nil {
				result.err = err
				return result
			}

			if _, err := m.collection(opt.kind).UpdateOne(ctx, bson.M{"uuid": opt.uuid}, bson.M{"$set": opt.attributes}); err != nil {
				result.err = err
				return result
			}

			return result
		},
	}
}

func (m *MongoDB) FindBy(ctx context.Context, opt findByOptions) Action {
	return Action{
		Closure: func() Result {
			var result findByResult

			if err := (*options)(opt).Validate(); err != nil {
				result.err = err
				return result
			}

			var record Record
			if err := m.collection(opt.kind).FindOne(ctx, opt.attributes.BSON()).Decode(&record); err != nil {
				result.err = err
				return result
			}

			result.record = &record

			return result
		},
	}
}

func (m *MongoDB) Delete(ctx context.Context, opt deleteOptions) Action {
	return Action{
		Closure: func() Result {
			var result deleteResult

			if err := (*options)(opt).Validate(); err != nil {
				result.err = err
				return result
			}

			if _, err := m.collection(opt.kind).DeleteOne(ctx, bson.M{"uuid": opt.uuid}); err != nil {
				result.err = err
				return result
			}

			return result
		},
	}
}
